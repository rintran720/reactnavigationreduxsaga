import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View, Text, TouchableHighlight, StyleSheet }  from 'react-native';
import Actions from '../Actions/Creators';

export class LoginScreen extends Component {
  static propTypes = {
    s: PropTypes.string
  }

  handleLogin = async () => {
    await this.props.login({
      username: 'this.state.username',
      password: 'this.state.password'
    });
  };

  render() {
    return (
      <View  style={styles.container}>
        <TouchableHighlight
         style={styles.button}
         onPress={this.handleLogin}
        >
         <Text> Touch Here </Text>
        </TouchableHighlight>
         
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  countContainer: {
    alignItems: 'center',
    padding: 10
  },
  countText: {
    color: '#FF00FF'
  }
})

const mapStateToProps = (state) => ({
  
})

const mapActionToProps = {
  login: Actions.login,
  // loginWithFacebook: Actions.loginWithFacebook,
  setUser: Actions.setUser
};

export default connect(mapStateToProps, mapActionToProps)(LoginScreen)
