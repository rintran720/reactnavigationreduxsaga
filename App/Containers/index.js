/*
* folder contain all screen of application
* base on redux
*/

import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";

import MainScreen from "./MainScreen";


export {
    LoginScreen,
    RegisterScreen,

    MainScreen
};