import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View, Text }  from 'react-native';

export class MainScreen extends Component {
  static propTypes = {
    s: PropTypes.string
  }

  render() {
    return (
        <View>
            <Text>Main</Text>
        </View>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
