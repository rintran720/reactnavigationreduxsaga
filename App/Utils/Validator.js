export const isPhoneNumber = (phoneNumber = "") => {
  let patt = new RegExp(/[0-9]+/g);
  return patt.test(phoneNumber);
};
