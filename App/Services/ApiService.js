import apisauce from "apisauce";

const naviMonitor = response => {
  console.log("API Response: ", response);
};

const create = (url, token) => {
  const option = {
    baseURL: url,
    headers: {
      "Cache-Control": "no-cache",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    timeout: 20000
  };
  if (token) {
    option.headers["access-token"] = token;
    option.headers.status = "ACTIVE";
  }

  const api = apisauce.create(option);

  // Use for developer
  if (process.env.NODE_ENV === "development") {
    api.addMonitor(naviMonitor);
  }

  return {
    get: api.get,
    post: api.post,
    put: api.put,
    patch: api.patch,
    delete: api.delete
  };
};

export default {
  create
};
