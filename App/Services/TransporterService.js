import ApiService from "./ApiService";
import HostSetting from "../Config/HostSettings";

function transporter(url, query, token = null) {
  return ApiService.create(HostSetting.getUrl(url, query), token);
}

export default transporter;
