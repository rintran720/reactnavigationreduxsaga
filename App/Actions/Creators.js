import Types from "./Types";

/*----------------------------------------------------------------------
 STARTUP
 ----------------------------------------------------------------------*/

const startup = () => ({ type: Types.STARTUP });

/*----------------------------------------------------------------------
AUTHENTICATION
----------------------------------------------------------------------*/

const login = payload => ({ type: Types.LOGIN_REQUEST, payload });

const loginWithFacebook = () => ({
  type: Types.LOGIN_WITH_FACEBOOK_REQUEST
});

const register = payload => ({ type: Types.REGISTER_REQUEST, payload });

const logout = () => ({ type: Types.LOGOUT_REQUEST });

const changePassword = payload => ({
  type: Types.CHANGE_PASSWORD_REQUEST,
  payload
});

/*----------------------------------------------------------------------
USER
----------------------------------------------------------------------*/

const setUser = data => ({ type: Types.SET_USER, data });

const clearUser = () => ({ type: Types.CLEAR_USER });

const changeLanguage = lang => ({ type: Types.CHANGE_LANGUAGE, lang });

/**
 Makes available all the action creators we've created.
 */
export default {
  startup,

  login,
  loginWithFacebook,
  register,
  logout,
  changePassword,

  setUser,
  clearUser,
  changeLanguage,

};
