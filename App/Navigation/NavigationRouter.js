import React, { Component } from "react";
import { Text, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import {
  StackNavigator,
  TabNavigator,
  TabBarBottom,
  DrawerNavigator,
  NavigationActions
} from "react-navigation";
// import Icon from "react-native-vector-icons/Ionicons";
// import I18n from "../I18n/I18n";
import * as Screen from "../Containers";
// import { Drawer } from "../Components";
// import { Metrics, Colors, Fonts, AppStyles } from "../Themes";
import { MainScreen } from "../Containers/MainScreen";

class NavigationRouter extends Component {
  render() {
    const { isLoggedIn, userLevel } = this.props;
    if (isLoggedIn) {
      if (userLevel === 1) return <UserStack />;
      // if (userLevel === 2) return <WaitressStack />;
      // if (userLevel === 3) return <CockerStack />;
      // if (userLevel === 4) return <ShishaMasterStack />;
      // if (userLevel === 9) return <AdminStack />;
    }
    return <AuthStack />;
  }
}

const AuthStack = StackNavigator(
  {
    Login: { screen: Screen.LoginScreen, navigationOptions: { header: null } },
    Register: {
      screen: Screen.RegisterScreen,
      navigationOptions: { header: null }
    },
  },
  {
    initialRouteName: "Login"
  }
);

/*
const HomeMainTabbar = TabNavigator(
  {
    Home: {
      screen: Screen.WelcomeScreen,
      navigationOptions: {
        title: I18n.t("home"),
        header: null
        // tabBarVisible: true
      }
    },
    Bill: {]
      screen: Screen.CartScreen,
      navigationOptions: { title: I18n.t("bill") }
    },
    QRScan: {
      screen: Screen.QRScanScreen,
      navigationOptions: { title: I18n.t("barCode") }
    },
    Partners: {
      screen: Screen.PartnersScreen,
      navigationOptions: { title: I18n.t("restaurent") }
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let icon;
        if (routeName === "Partners") {
          icon = (
            <Image
              source={require(`../Images/icons/002-serving-dish.png`)}
              style={{ tintColor, width: 30, height: 28 }}
            />
          );
        } else if (routeName === "QRScan") {
          icon = (
            <Image
              source={require(`../Images/icons/004-qr-code.png`)}
              style={{ tintColor, width: 30, height: 28 }}
            />
          );
        } else if (routeName === "Home") {
          icon = (
            <Image
              source={require(`../Images/icons/003-home.png`)}
              style={{ tintColor, width: 30, height: 28 }}
            />
          );
        } else if (routeName === "Bill") {
          icon = (
            <Image
              source={require(`../Images/icons/001-bill.png`)}
              style={{ tintColor, width: 30, height: 28 }}
            />
          );
        }
        return icon;
      }
    }),
    tabBarOptions: {
      activeTintColor: Colors.dark,
      inactiveTintColor: Colors.branding
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: "bottom"
    // animationEnabled: false,
    // swipeEnabled: false,
  }
);

const UserStack = StackNavigator(
  {
    HomeMain: { screen: HomeMainTabbar },
    Menu: {
      screen: Screen.MenuScreen,
      navigationOptions: { title: I18n.t("menu") }
    },
    PartnerDetail: {
      screen: Screen.PartnerDetailScreen,
      navigationOptions: {
        headerStyle: {
          backgroundColor: "transparent"
        }
      }
    },
    Allergens: {
      screen: Screen.AllergensScreen,
      navigationOptions: { title: I18n.t("allergens") }
    },
    Cart: {
      screen: Screen.CartScreen,
      navigationOptions: { title: I18n.t("cart") }
    },
    ViewCart: { screen: Screen.ViewCartScreen },
    OrderHistory: {
      screen: Screen.OrderHistoryScreen,
      navigationOptions: { title: I18n.t("orderHistory") }
    },
    Payment: {
      screen: Screen.PaymentScreen,
      navigationOptions: {
        title: I18n.t("tableOrder"),
        headerStyle: {
          backgroundColor: "transparent"
        }
      }
    }
  },
  {
    initialRouteName: "HomeMain",
    navigationOptions: {
      headerStyle: {
        backgroundColor: Colors.branding
      },
      headerTintColor: Colors.light
    }
  }
);
*/

const UserStack = StackNavigator(
  {
    Main: { screen: MainScreen },
  }
);

const mapStateToProps = state => ({
  isLoggedIn: state.auth.data.isLoggedIn,
  userLevel: state.user.data.userLevel
});

export default connect(mapStateToProps)(NavigationRouter);
