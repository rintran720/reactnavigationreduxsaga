const HOST = {
   getUrl: (route='', query='') =>
    `https://qtkn.herokuapp.com/api/${route}${query}`
};

export default HOST;


