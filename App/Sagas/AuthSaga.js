import { takeLatest, put, call, select } from "redux-saga/effects";
import { Alert } from "react-native";
// import { LoginManager, AccessToken } from "react-native-fbsdk";
import Types from "../Actions/Types";

// Apis
import {
  login,
  // loginWithFacebook,
  // getFacebookUserInfomationByToken,
  register,
  logout,
  changepassword
} from "../Apis/AuthApi";

// login
export function* watchLogin(action) {
  const { username, password } = action.payload;
  try {
    if (!username || !password) {
      Alert.alert(null, "completeAllInformation");
      yield put({ type: Types.LOGIN_FAILURE });
      return null;
    }
    if (username && password) {
      const response = yield call(login, username, password);
      if (response.data && response.data.status === 1) {
        yield put({ type: Types.LOGIN_SUCCESS });
        yield put({ type: Types.SET_USER, data: response.data });
      } else if (response && response.problem === "NETWORK_ERROR") {
        Alert.alert(null, "networkError");
        yield put({ type: Types.LOGIN_FAILURE });
      } else {
        const error = response.data.error_fields.email_password;
        Alert.alert(null, "loginFailure");
        yield put({ type: Types.LOGIN_FAILURE });
      }
    }
  } catch (e) {
    console.log("Saga", e);
    yield put({ type: Types.LOGIN_FAILURE });
  }
}

// export function* loginWithFacebookSaga() {
//   try {
//     // get facebook access token
//     let data = yield AccessToken.getCurrentAccessToken();
//     if (data) {
//       const fbAccessToken = yield data.accessToken.toString();
//       const fbId = yield data.userID.toString();
//       yield call(fetchFbInformation, fbAccessToken, fbId);
//     }
//     if (!data) {
//       const response = yield LoginManager.logInWithReadPermissions([
//         "email",
//         "public_profile",
//         ""
//       ]);
//       if (response) {
//         let data = yield AccessToken.getCurrentAccessToken();
//         const fbAccessToken = yield data.accessToken.toString();
//         const fbId = yield data.userID.toString();
//         yield call(fetchFbInformation, fbAccessToken, fbId);
//       }
//     }
//   } catch (e) {
//     console.log("Saga", e);
//     yield put({ type: Types.LOGIN_WITH_FACEBOOK_FAILURE });
//   }
// }

// function* fetchFbInformation(fbAccessToken, fbId) {
//   const userFbInformation = yield call(
//     getFacebookUserInfomationByToken,
//     fbAccessToken
//   );

//   const { email } = userFbInformation;
//   const response = yield call(loginWithFacebook, fbId, email);

//   if (response.data && response.data.status === 1) {
//     yield put({
//       type: Types.LOGIN_WITH_FACEBOOK_SUCCESS,
//       data: { accessToken: fbAccessToken }
//     });
//     yield put({ type: Types.SET_USER, data: { userlevel: 1 } });
//   }
// }

// register

export function* watchRegister(action) {
  const { username, password, city, cb } = action.payload;
  try {
    if (!username || !password || !city) {
      Alert.alert(null, "completeAllInformation");
      yield put({ type: Types.LOGIN_FAILURE });
    }
    if (username && password && city) {
      const response = yield call(register, username, password, city);
      if (response && response.data !== null && response.data.status === 1) {
        yield put({ type: Types.REGISTER_SUCCESS });
        yield cb.navigate("Login");
        Alert.alert(null, "registerSuccess");
      } else if (response && response.problem === "NETWORK_ERROR") {
        Alert.alert(null, "networkError");
        yield put({ type: Types.REGISTER_FAILURE });
      } else {
        const errorEmail =
          response.data.error_fields[
            Object.keys(response.data.error_fields)[0]
          ];
        const errorPassword =
          response.data.error_fields[
            Object.keys(response.data.error_fields)[1]
          ];
        Alert.alert(null, errorEmail || errorPassword);
        yield put({ type: Types.REGISTER_FAILURE });
      }
    }
  } catch (e) {
    console.log("Saga", e);
    yield put({ type: Types.REGISTER_FAILURE });
  }
}

// logout
export function* watchLogout(action) {
  try {
    const response = yield call(logout);
    //yield LoginManager.logOut();
    // logout success
    if (response.data && response.data.status === 1) {
      yield put({ type: Types.LOGOUT_SUCCESS });
      yield put({ type: Types.SET_USER, data: { userlevel: 0 } });
      yield put({ type: Types.CLEAR_USER });
    } else if (response && response.problem === "NETWORK_ERROR") {
      Alert.alert(null, "networkError");
      yield put({ type: Types.LOGOUT_FAILURE });
    } else {
      Alert.alert(null, "logoutFailure");
      yield put({ type: Types.LOGOUT_FAILURE });
    }
  } catch (e) {
    console.log("Saga", e);
    yield put({ type: Types.LOGOUT_FAILURE });
  }
}

export function* watchChangePassword(action) {
  const { npassword } = action.payload;
  try {
    const response = yield call(changepassword, npassword);
    // register success
    if (response && response.data !== null && response.data.status === 1) {
      Alert.alert(null, "changePasswordSuccess");
      yield put({ type: Types.CHANGE_PASSWORD_SUCCESS });
    } else if (response && response.problem === "NETWORK_ERROR") {
      Alert.alert(null, "networkError");
      yield put({ type: Types.CHANGE_PASSWORD_FAILURE });
    } else {
      Alert.alert(null, "changePasswordFailure");
      yield put({ type: Types.CHANGE_PASSWORD_FAILURE });
    }
  } catch (e) {
    console.log("Saga", e);
    yield put({ type: Types.CHANGE_PASSWORD_FAILURE });
  }
}

// process Auth actions
export function* watchAuth() {
  yield takeLatest(Types.LOGIN_REQUEST, watchLogin);
  // yield takeLatest(Types.LOGIN_WITH_FACEBOOK_REQUEST, loginWithFacebookSaga);
  yield takeLatest(Types.REGISTER_REQUEST, watchRegister);
  yield takeLatest(Types.LOGOUT_REQUEST, watchLogout);
  yield takeLatest(Types.CHANGE_PASSWORD_REQUEST, watchChangePassword);
}
