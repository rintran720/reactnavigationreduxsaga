import { fork, all } from "redux-saga/effects";

import { watchStartup } from "./StartupSaga";
import { watchAuth } from "./AuthSaga";


/** ************* */

import DebugSettings from "../Config/DebugSettings";

/* Create our API at this level and feed it into
  the sagas that are expected to make API calls
  so there's only 1 copy app-wide!
*/

// start the daemons
export default function* root() {
  yield all([
    fork(watchStartup),
    fork(watchAuth),
    
  ]);
}
