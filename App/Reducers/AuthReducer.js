import Immutable from "seamless-immutable";
import { createReducer } from 'reduxsauce';
import Types from "../Actions/Types";

export const INITIAL_STATE = Immutable({
  data: {
    isLoggedIn: false,
    accessTokenFacebook: null
  },
  login: {
    isFetching: false
  },
  loginWithFacebook: {
    isFetching: false
  },
  register: {
    isFetching: false
  },
  logout: {
    isFetching: false
  }
});

const LOGIN_REQUEST = state => state.merge({ login: { isFetching: true } });

const LOGIN_SUCCESS = (state, action) =>
  state.merge({ login: { isFetching: false }, data: { isLoggedIn: true } });

const LOGIN_FAILURE = state => state.merge({ login: { isFetching: false } });

const LOGIN_WITH_FACEBOOK_REQUEST = state =>
  state.merge({ loginWithFacebook: { isFetching: true } });

const LOGIN_WITH_FACEBOOK_SUCCESS = (state, action) =>
  state.merge({
    loginWithFacebook: { isFetching: false },
    data: { isLoggedIn: true, accessTokenFacebook: action.data.accessToken }
  });

const LOGIN_WITH_FACEBOOK_FAILURE = state =>
  state.merge({ loginWithFacebook: { isFetching: false } });

const REGISTER_REQUEST = state =>
  state.merge({ register: { isFetching: true } });

const REGISTER_SUCCESS = (state, action) =>
  state.merge({ register: { isFetching: false }, data: { isLoggedIn: true } });

const REGISTER_FAILURE = state =>
  state.merge({ register: { isFetching: false } });

const CHANGE_PASSWORD_SUCCESS = (state, action) =>
  state.merge({ data: { isLoggedIn: true } });

const LOGOUT_SUCCESS = (state, action) =>
  state.merge({ data: { isLoggedIn: false } });

const CLEAR_USER = () => INITIAL_STATE;

const ACTION_HANDLERS = {
  [Types.LOGIN_REQUEST]: LOGIN_REQUEST,
  [Types.LOGIN_SUCCESS]: LOGIN_SUCCESS,
  [Types.LOGIN_FAILURE]: LOGIN_FAILURE,

  [Types.LOGIN_WITH_FACEBOOK_REQUEST]: LOGIN_WITH_FACEBOOK_REQUEST,
  [Types.LOGIN_WITH_FACEBOOK_SUCCESS]: LOGIN_WITH_FACEBOOK_SUCCESS,
  [Types.LOGIN_WITH_FACEBOOK_FAILURE]: LOGIN_WITH_FACEBOOK_FAILURE,

  [Types.LOGOUT_SUCCESS]: LOGOUT_SUCCESS,

  [Types.REGISTER_REQUEST]: REGISTER_REQUEST,
  [Types.REGISTER_SUCCESS]: REGISTER_SUCCESS,
  [Types.REGISTER_FAILURE]: REGISTER_FAILURE,

  [Types.CHANGE_PASSWORD_SUCCESS]: CHANGE_PASSWORD_SUCCESS,

  [Types.CLEAR_USER]: CLEAR_USER
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
