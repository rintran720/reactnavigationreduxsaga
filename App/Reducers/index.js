import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import UserReducer from "./UserReducer";


// glue all the reducers together into 1 root reducer
const appReducer = combineReducers({
  auth: AuthReducer,
  user: UserReducer,

});

const rootReducer = (state, action) => {
  if (action.type === "LOGOUT_REQUEST") {
    Object.keys(state).forEach(key => {
      const data = state[key].data;
      return data === [];
    });
  }
  return appReducer(state, action);
};

export default rootReducer;

// Put reducer keys that you do NOT want stored to persistence here
export const persistentStoreBlacklist = [
  
];

// OR put reducer keys that you DO want stored to persistence here (overrides blacklist)
export const persistentStoreWhitelist = ["auth", "user"];
