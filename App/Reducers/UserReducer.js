import Immutable from "seamless-immutable";
import { createReducer } from "reduxsauce";
import Types from "../Actions/Types";

export const INITIAL_STATE = Immutable({
  data: {
    userLevel: 0
  },
  language: ""
});

const SET_USER = (state, action) =>
  state.merge({
    data: { ...state.data, userLevel: Number(action.data.userlevel) }
  });

const CHANGE_LANGUAGE = (state, action) =>
  state.merge({
    language: action.lang
  });

const CLEAR_USER = (state, action) => INITIAL_STATE;

const ACTION_HANDLERS = {
  [Types.SET_USER]: SET_USER,
  [Types.CLEAR_USER]: CLEAR_USER,
  [Types.CHANGE_LANGUAGE]: CHANGE_LANGUAGE
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
