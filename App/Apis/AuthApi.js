import TransporterService from "../Services/TransporterService";

function* login(username, password) {
  try {
    let data = new FormData();
    data.append("login", 1);
    data.append("email", username);
    data.append("password", password);
    const response = yield TransporterService("/user/login.php").post("", data);
    return response;
  } catch (error) {
    console.log("API", error);
  }
}

function* loginWithFacebook(fbId, email) {
  try {
    let data = new FormData();
    data.append("fbid", fbId);
    data.append("email", email);
    const response = yield TransporterService("/user/fblogin.php").post(
      "",
      data
    );
    return response;
  } catch (error) {
    console.log("API", error);
  }
}

function* getFacebookUserInfomationByToken(fbAccessToken) {
  try {
    const response = yield fetch(
      `https://graph.facebook.com/me?fields=id,name,email,first_name,last_name,gender,picture,cover&access_token=${fbAccessToken}`
    );
    let userData = yield response.json();
    return userData;
  } catch (error) {
    console.log(error);
  }
}

function* register(username, password, city) {
  try {
    let data = new FormData();
    data.append("register", 1);
    data.append("email", username);
    data.append("password", password);
    data.append("city", city);
    const response = yield TransporterService("/user/register.php").post(
      "",
      data
    );
    return response;
  } catch (error) {
    console.log("API", error);
  }
}

function* logout() {
  try {
    let data = new FormData();
    data.append("logout", 1);
    const response = yield TransporterService("/user/logout.php").post(
      "",
      data
    );
    return response;
  } catch (error) {
    console.log("API", error);
  }
}

function* changepassword(password) {
  try {
    const response = yield TransporterService("/user/profile.php").post("", {
      changePassword: 1,
      password,
      confirm_password: password
    });
    return response;
  } catch (error) {
    console.log("API", error);
  }
}

export {
  login,
  loginWithFacebook,
  getFacebookUserInfomationByToken,
  register,
  logout,
  changepassword
};
