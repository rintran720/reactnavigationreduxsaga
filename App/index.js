import React from "react";
import { connect } from "react-redux";
import NavigationRouter from "./Navigation/NavigationRouter";


class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    //I18n.locale = nextProps.lang;
  }

  render() {
    return <NavigationRouter {...this.props} />;
  }
}

const mapStateToProps = state => ({
  //lang: state.user.language
});

export default connect(mapStateToProps)(App);
