
import React from "react";
import { AppRegistry } from "react-native";
import { Provider } from "react-redux";
import { YellowBox } from "react-native";

import App from "./App";
import configureStore from "./App/Store/Store";

YellowBox.ignoreWarnings([
    "Warning: isMounted(...) is deprecated",
    "Module RCTImageLoader",
    "Remote debugger is in a background tab which may cause apps to perform slowly. Fix this by foregrounding the tab (or opening it in a separate window)"
  ]);
  
  const store = configureStore();
  
  class Root extends React.Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Provider store={store}>
          <App {...this.props} />
        </Provider>
      );
    }
  }

  AppRegistry.registerComponent("ReactNavigationReduxSaga", () => Root);